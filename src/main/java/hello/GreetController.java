package hello;

import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Get;

@Controller("/greet")
public class GreetController {

    @Get("/")
    public String greet() {
        return "hello world";
    }
}
