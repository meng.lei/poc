#!/usr/bin/env bash

#
# Script to initialize preparations for the CI pipeline

#------------------------------------------------------------------------------
# Globals
#   CI_COMMIT_TAG
#   CI_COMMIT_REF_NAME
#   GRADLE_PROPERTIES_FILE
#   SONAR_MASTER_URL
#   SONAR_MASTER_LOGIN
#   SONAR_DEV_URL
#   SONAR_DEV_LOGIN
#------------------------------------------------------------------------------
CI_COMMIT_TAG="${CI_COMMIT_TAG:-}"
CI_COMMIT_REF_NAME="${CI_COMMIT_REF_NAME:-}"
GRADLE_PROPERTIES_FILE="${GRADLE_PROPERTIES_FILE:-}"
SONAR_MASTER_URL="${SONAR_MASTER_URL:-}"
SONAR_MASTER_LOGIN="${SONAR_MASTER_LOGIN:-}"
SONAR_DEV_URL="${SONAR_DEV_URL:-}"
SONAR_DEV_LOGIN="${SONAR_DEV_LOGIN:-}"

###############################################################################
# Print out Globals
###############################################################################
print_globals() {
    echo "CI_COMMIT_TAG = $CI_COMMIT_TAG
CI_COMMIT_REF_NAME = $CI_COMMIT_REF_NAME
GRADLE_PROPERTIES_FILE = ${GRADLE_PROPERTIES_FILE}
SONAR_MASTER_URL = ${SONAR_MASTER_URL}
SONAR_DEV_URL = ${SONAR_DEV_URL}
"
}


###############################################################################
# Script starts here
###############################################################################

echo "Checking Globals variables CI_COMMIT_TAG: (${CI_COMMIT_TAG})"
echo "Checking Globals variables CI_COMMIT_REF_NAME: (${CI_COMMIT_REF_NAME})"
echo "Checking Globals variables GRADLE_PROPERTIES_FILE: (${GRADLE_PROPERTIES_FILE})"
